
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>

__device__ int dev_sort_array[128 * 1024];
__device__ int dev_array_size;
__device__ int BLOCK_COUNT;

__global__ void Bitonic_sort_x2048elements()  // bool param�ter sz�ks�ges lehet a nagyobb elemsz�m� cser�kn�l
{
	__shared__ int shr_1024elements_first[64][32];

	int i = (blockIdx.x * 1024 + threadIdx.x * 32 + threadIdx.y) + blockIdx.x * 1024;
	int this_idx = threadIdx.x * 32 + threadIdx.y;
	int helper;

	//copy device --> shared
	shr_1024elements_first[threadIdx.x][threadIdx.y] = dev_sort_array[i];
	shr_1024elements_first[threadIdx.x + 32][threadIdx.y] = dev_sort_array[i + 1024];
	//
	__syncthreads();

	for (int j = 1; j <= 1024; j = j * 2)
	{

		for (int k = j; k >= 1; k = k / 2)
		{
			int shift = (this_idx / k) * k;
			int shift_x = shift / 32;
			int shift_y = shift % 32;
			int k_x = k / 32;
			int k_y = k % 32;
			if (blockIdx.x % 2 == 0)
			{
				if ((this_idx / j) % 2 == 0)
				{
					if (shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] > shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y])
					{
						helper = shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y];
						shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] = shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y];
						shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y] = helper;
					}
				}
				else
				{
					if (shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] < shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y])
					{
						helper = shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y];
						shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] = shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y];
						shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y] = helper;
					}
				}
			}
			else
			{
				if ((this_idx / j) % 2 == 0)
				{
					if (shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] < shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y])
					{
						helper = shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y];
						shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] = shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y];
						shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y] = helper;
					}
				}
				else
				{
					if (shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] > shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y])
					{
						helper = shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y];
						shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] = shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y];
						shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y] = helper;
					}
				}
			}
			__syncthreads();
		}

		__syncthreads();
	}
	__syncthreads();

	dev_sort_array[i] = shr_1024elements_first[threadIdx.x][threadIdx.y];
	dev_sort_array[i + 1024] = shr_1024elements_first[threadIdx.x + 32][threadIdx.y];

	__syncthreads();
}

__global__ void Bitonic_sort_x2048elements2(int maxtavolsag, int tavolsag)
{
	__shared__ int shr_1024elements_first[64][32];

	int i = (blockIdx.x * 1024 + threadIdx.x * 32 + threadIdx.y) + blockIdx.x * 1024;
	int this_idx = threadIdx.x * 32 + threadIdx.y;
	int helper;

	//copy device --> shared
	shr_1024elements_first[threadIdx.x][threadIdx.y] = dev_sort_array[i];
	shr_1024elements_first[threadIdx.x + 32][threadIdx.y] = dev_sort_array[i + 1024];
	//
	__syncthreads();

	for (int j = tavolsag; j <= 1024; j = j * 2)
	{

		for (int k = j; k >= 1; k = k / 2)
		{
			int shift = (this_idx / k) * k;
			int shift_x = shift / 32;
			int shift_y = shift % 32;
			int k_x = k / 32;
			int k_y = k % 32;
			if (((i - blockIdx.x * 1024) / maxtavolsag) % 2 == 0)
			{
				if (shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] > shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y])
				{
					helper = shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y];
					shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] = shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y];
					shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y] = helper;
				}
			}
			else
			{
				if (shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] < shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y])
				{
					helper = shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y];
					shr_1024elements_first[threadIdx.x + shift_x][threadIdx.y + shift_y] = shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y];
					shr_1024elements_first[threadIdx.x + shift_x + k_x][threadIdx.y + shift_y + k_y] = helper;
				}
			}
			__syncthreads();
		}

		__syncthreads();
	}
	__syncthreads();

	dev_sort_array[i] = shr_1024elements_first[threadIdx.x][threadIdx.y];
	dev_sort_array[i + 1024] = shr_1024elements_first[threadIdx.x + 32][threadIdx.y];

	__syncthreads();
}

__global__ void Bitonic_sort_allElements(int Maxtavolsag, int tavolsag)
{

	__shared__ int shr_1024elements_first[32][32];
	__shared__ int shr_1024elements_second[32][32];

	int eltol = blockIdx.x / (tavolsag / 1024);
	int i = (blockIdx.x * 1024 + threadIdx.x * 32 + threadIdx.y) + eltol * tavolsag;
	int helper;

	shr_1024elements_first[threadIdx.x][threadIdx.y] = dev_sort_array[i];
	shr_1024elements_second[threadIdx.x][threadIdx.y] = dev_sort_array[i + tavolsag];

	__syncthreads();


	if (((i - eltol * tavolsag) / Maxtavolsag) % 2 == 0)
	{
		if (shr_1024elements_first[threadIdx.x][threadIdx.y] > shr_1024elements_second[threadIdx.x][threadIdx.y])
		{
			helper = shr_1024elements_first[threadIdx.x][threadIdx.y];
			shr_1024elements_first[threadIdx.x][threadIdx.y] = shr_1024elements_second[threadIdx.x][threadIdx.y];
			shr_1024elements_second[threadIdx.x][threadIdx.y] = helper;
		}
	}
	else
	{
		if (shr_1024elements_first[threadIdx.x][threadIdx.y] < shr_1024elements_second[threadIdx.x][threadIdx.y])
		{
			helper = shr_1024elements_first[threadIdx.x][threadIdx.y];
			shr_1024elements_first[threadIdx.x][threadIdx.y] = shr_1024elements_second[threadIdx.x][threadIdx.y];
			shr_1024elements_second[threadIdx.x][threadIdx.y] = helper;
		}
	}

	__syncthreads();

	dev_sort_array[i] = shr_1024elements_first[threadIdx.x][threadIdx.y];
	dev_sort_array[i + tavolsag] = shr_1024elements_second[threadIdx.x][threadIdx.y];

	__syncthreads();
}

#pragma region QuickSortMethods

void exchange(int data[], int m, int n)
{
	int temporary;

	temporary = data[m];
	data[m] = data[n];
	data[n] = temporary;
}

void IntArrayQuickSort(int data[], int l, int r)
{
	int i, j;
	int x;

	i = l;
	j = r;

	x = data[(l + r) / 2]; /* find pivot item */
	while (true) {
		while (data[i] < x)
			i++;
		while (x < data[j])
			j--;
		if (i <= j) {
			exchange(data, i, j);
			i++;
			j--;
		}
		if (i > j)
			break;
	}
	if (l < j)
		IntArrayQuickSort(data, l, j);
	if (i < r)
		IntArrayQuickSort(data, i, r);
}

#pragma endregion

int main()
{
	int sortarray[128 * 1024];
	int array_size = sizeof(sortarray) / sizeof(int);
	int c = array_size / 2048;
	for (int i = 0; i < array_size; i++)
	{
		sortarray[i] = rand() % 10000;
	}

	int* dev_ptr;
	cudaMalloc((void**)&dev_ptr, array_size);

	cudaMemcpyToSymbol(dev_array_size, &array_size, sizeof(int));
	cudaMemcpyToSymbol(dev_sort_array, sortarray, array_size * sizeof(int));

	float elapsed = 0;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);

	Bitonic_sort_x2048elements << < c, dim3(32, 32) >> > ();

	for (int i = 2048; i <= 64 * 1024; i = i * 2)
	{
		for (int j = i; j >= 1024; j = j / 2)
		{
			if (j > 1024)
			{
				Bitonic_sort_allElements << <64, dim3(32, 32) >> > (i, j);
			}

			if (j == 1024)
			{
				Bitonic_sort_x2048elements2 << <64, dim3(32, 32) >> > (i, 1024);
			}

		}
	}

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsed, start, stop);
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	cudaMemcpyFromSymbol(sortarray, dev_sort_array, array_size * sizeof(int));
	printf("A GPU-n parhuzamositott rendezes futasi ideje: %.5f ms", elapsed);

#pragma region CPUSorts

	for (int i = 0; i < array_size; i++)
	{
		sortarray[i] = rand() % 10000;
	}
	clock_t startTime, endTime;
	double elapsedtime = 0;
	startTime = clock();

	for (int i = 0; i < array_size - 1; i++)
	{
		for (int j = i + 1; j < array_size; j++)
		{
			if (sortarray[i] > sortarray[j]) {
				int a = sortarray[i];
				sortarray[i] = sortarray[j];
				sortarray[j] = a;
			}
		}
	}

	endTime = clock();
	elapsedtime = (endTime - startTime);
	printf("\nA CPU-n egyszer� cseres rendezes futasi ideje: %.5f ms", elapsedtime);

	//---------------------------------------------------------------------------

	for (int i = 0; i < array_size; i++)
	{
		sortarray[i] = rand() % 10000;
	}
	elapsedtime = 0;
	startTime = clock();

	for (int i = array_size; i > 0; i--)
	{
		for (int j = 0; j < array_size - 1; j++)
		{
			if (sortarray[j] > sortarray[j + 1]) {
				int a = sortarray[j + 1];
				sortarray[j + 1] = sortarray[j];
				sortarray[j] = a;
			}
		}
	}

	endTime = clock();
	elapsedtime = (endTime - startTime);
	printf("\nA CPU-n buborek rendezes futasi ideje: %.5f ms", elapsedtime);

	//--------------------------------------------------------------------

	for (int i = 0; i < array_size; i++)
	{
		sortarray[i] = rand() % 10000;
	}
	elapsedtime = 0;
	startTime = clock();

	for (int i = 0; i < array_size - 1; i++)
	{
		int min = i;
		for (int j = i + 1; j < array_size; j++)
		{
			if (sortarray[min] > sortarray[j]) {
				min = j;
			}
			int a = sortarray[min];
			sortarray[min] = sortarray[i];
			sortarray[i] = a;
		}
	}

	endTime = clock();
	elapsedtime = (endTime - startTime);
	printf("\nA CPU-n minimum kivalasztasos rendezes futasi ideje: %.5f ms", elapsedtime);

	//--------------------------------------------------------------------

	for (int i = 0; i < array_size; i++)
	{
		sortarray[i] = rand() % 10000;
	}
	elapsedtime = 0;
	startTime = clock();

	for (int i = 0; i < array_size; i++)
	{
		int j = i - 1;
		int temp = sortarray[i];
		while (j >= 0 && sortarray[j] > temp)
		{
			sortarray[j + 1] = sortarray[j];
			j = j - 1;
		}
		sortarray[j + 1] = temp;
	}

	endTime = clock();
	elapsedtime = (endTime - startTime);
	printf("\nA CPU-n tovabbfejlesztett bubor�k rendezes futasi ideje: %.5f ms", elapsedtime);

	//--------------------------------------------------------------------

	for (int i = 0; i < array_size; i++)
	{
		sortarray[i] = rand() % 10000;
	}
	elapsedtime = 0;
	startTime = clock();


	IntArrayQuickSort(sortarray, 0, array_size - 1);

	endTime = clock();
	elapsedtime = (endTime - startTime);
	printf("\nA CPU-n gyors rendezes futasi ideje: %.5f ms", elapsedtime);

#pragma endregion
}



